Laboratorio de WebServices

**Elementos a utilizar**
* JavaEE, JDK 1.8
* IDE Eclipse, elegir la opción Eclipse for Java EE
* IDE Eclipse, instalar Jboss Tools (Menú: Help -> Eclipse MarketPlace. Buscar "Jboss Tools"). Este paso solo es necesario para crear un proyecto nuevo.
* Instalar Jboss tools 
* Instalar SOAP-UI
* Instalar PostMan para Google Chrome
* Instalar el motor de base de datos Postgresql

**Descargar el proyecto**

Via SSH:
    
    git clone git@gitlab.com:enriq16/personas.git
    
Via HTTPS:
    
    git clone https://gitlab.com/enriq16/personas.git
    
    
**Importar proyecto**
 * Abrir el IDE Eclipse
 * Menú File -> Import -> Existing Maven Project
 * El IDE comenzará a importar las librerías de Maven
 
**Base de datos**
**Estructura de la Base de Datos utilizada**
CREATE TABLE public.persona_asignatura
(
  id integer NOT NULL DEFAULT nextval('nombretabla_id_seq'::regclass),
  id_persona integer NOT NULL,
  id_asignatura integer NOT NULL,
  CONSTRAINT fk_asignatura FOREIGN KEY (id_asignatura)
      REFERENCES public.asignatura (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_persona FOREIGN KEY (id_persona)
      REFERENCES public.persona (cedula) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.persona_asignatura
  OWNER TO postgres;



CREATE TABLE public.asignatura
(
  id integer NOT NULL DEFAULT nextval('asignaturas_id_seq'::regclass),
  nombre character varying(100) NOT NULL,
  CONSTRAINT pk_asignatura PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.asignatura
  OWNER TO postgres;

CREATE TABLE public.persona
(
  cedula integer NOT NULL,
  nombre character varying(1000),
  apellido character varying(1000),
  CONSTRAINT pk_cedula PRIMARY KEY (cedula)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.persona
  OWNER TO postgres;



**Verificación del código fuente**
 

**Deployar en Servidor**
 * Desde el IDE Eclipse, configurar el servidor de aplicaciones Wildfly10 (Verificar la guía de clase anterior sobre el laboratorio de JavaRMI)
 * Deploy del proyecto "personas" en el servidor Wildfly


**Verificación con herramientas: POSTMAN / SOAP-UI / Browser**
 * GET http://localhost:8080/personas/rest/personas
 * GET http://localhost:8080/personas/rest/personas/3298639
 * GET http://localhost:8080/personas/rest/personas/cedula?cedula=160160
 * POST http://localhost:8080/personas/rest/personas/
 * DELETE http://localhost:8080/personas/rest/personas/150150
 * Imágenes de ejemplo de utilización de POSTMAN para la petición POST en el directorio: https://gitlab.com/fmancia/sd/tree/master/lab-ws/cliente-postman
 

**Tarea**
** Observacion: Toadas las funcionalidades pedidas estan implementadas **
 * Modificar el ejemplo del profesor para incluir más funcionalidades.
 * Se deben crear servicios RESTful necesarios para la administración de "asignaturas"
 * Las asignaturas están asociadas a una persona.
 * Pueden estar en memoria o en base de datos.
 * Los servicios deben poder:
    * crear/modificar/listar/borrar asignaturas.
    * asociar y desasociar una asignatura a una persona.
    * listar todas las asignaturas de una persona.
    * listar todos los alumnos de una asignatura.
 * Se debe entregar de la siguiente forma:
     * Enviar un email al profesor:  fernandomancia@gmail.com 
     * El email debe contener el link del repositorio personal del alumn@
     * El email debe contener imagenes en PNG o JPG de los prints de pantalla de las pruebas a los restful solicitados.
     * En el link del repositorio gitlab del alumn@ debe estar el código fuente de la tarea solicitada.
     * Se debe entregar a más tardar el Lunes 07/05/2018





